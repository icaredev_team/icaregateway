package com.icare.gateway;

import com.google.common.util.concurrent.ListenableFuture;
import com.icare.gateway.dao.Annonce;
import com.icare.gateway.dao.Conducteur;
import com.icare.gateway.wrapper.AnnonceWrapper;
import feign.hystrix.FallbackFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.binding.StreamListenerAnnotationBeanPostProcessor;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.*;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.integration.endpoint.EventDrivenConsumer;
import org.springframework.integration.handler.BridgeHandler;
import org.springframework.integration.handler.DelayHandler;
import org.springframework.integration.handler.MessageProcessor;
import org.springframework.messaging.*;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


@SpringBootApplication(scanBasePackageClasses = ICareGatewayApplication.class)
@EnableBinding(MessageStream.class)
@EnableDiscoveryClient
@EnableIntegration
@IntegrationComponentScan("com.icare.gateway")
@EnableFeignClients
@RestController
@RequestMapping("/icare")
public class ICareGatewayApplication {

	@Autowired
	private MessageStream messageStream;


	@Autowired
	private AnnonceFeignClient annonceFeignClient;




	@RequestMapping(method = RequestMethod.POST, value = "/annonces")
	public void addAnnonce(@RequestBody AnnonceWrapper annonceWrapper ) {
		System.out.println(annonceWrapper.toString());
		final Message<AnnonceWrapper> message = MessageBuilder
														.withPayload(annonceWrapper)
														.build();
		messageStream.addAnnonceOutput().send(message);
		messageStream.addAnnonceInput().subscribe(message1 -> System.out.println(message1.getPayload().toString()));

	}

	@RequestMapping(method = RequestMethod.POST, value = "/annonces/demande/{demandeId}")
	public void acceptDemande(@PathVariable Long demandeId) {
		Message<Long> message = MessageBuilder
												  .withPayload(demandeId)
												  .setReplyChannelName(MessageStream.ACCEPT_DEMANDE_INPUT)
												  .build();
		this.messageStream.acceptDemandeOutput().send(message);
	}


	@RequestMapping(method = RequestMethod.GET, value = "/annonces/{annonceId}")
	public Annonce getAnnonceClients(@PathVariable Long annonceId) {
		return annonceFeignClient.getAnnonceById(annonceId);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/annonces")
	public List<Annonce> getAllAnnonces() {
		return annonceFeignClient.getAllAnnonces();
	}


	@RequestMapping(method = RequestMethod.PUT, value = "/conducteurs")
	public void updateConducteur(@RequestBody  Conducteur conducteur) {
		System.out.println("===>" + conducteur.toString());
		Message<Conducteur> message = MessageBuilder
										.withPayload(conducteur)
										.setReplyChannelName(MessageStream.UPDATE_CONDUCTEUR_OUTPUT)
										.build();
		this.messageStream.updateConducteurOutput().send(message);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/conducteurs")
	public void addConducteur(@RequestBody  Conducteur conducteur) {
		System.out.println("===>" + conducteur.toString());
		Message<Conducteur> message = MessageBuilder
											  .withPayload(conducteur)
											  .setReplyChannelName(MessageStream.ADD_CONDUCTEUR_OUTPUT)
											  .build();
		this.messageStream.addConducteurOutput().send(message);
	}




	public static void main(String[] args) {
		SpringApplication.run(ICareGatewayApplication.class, args);
	}

}

@FeignClient(value = "annonce-service", fallbackFactory = AnnonceFeignClientHystrixFallback.class)
 interface AnnonceFeignClient {
	@RequestMapping(method = RequestMethod.GET, value = "/annonces/{annonceId}")
	Annonce getAnnonceById(@PathVariable("annonceId") Long annonceId);
	@RequestMapping(method = RequestMethod.GET, value = "/annonces")
	List<Annonce> getAllAnnonces();
}






@Component
class AnnonceFeignClientHystrixFallback implements FallbackFactory<AnnonceFeignClient> {

	@Override
	public AnnonceFeignClient create(Throwable cause) {
		return new AnnonceFeignClient() {
			@Override
			public Annonce getAnnonceById(Long annonceId) {
				cause.printStackTrace();
				return null;
			}

			@Override
			public List<Annonce> getAllAnnonces() {
				cause.printStackTrace();
				return new ArrayList<>();
			}
		};
	}
}



