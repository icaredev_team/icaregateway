package com.icare.gateway;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created by ahadri on 1/30/17.
 */

@Configuration
public class Config {


	//@Input(ADD_ANNONCE_INPUT)
	@Bean
	MessageChannel addAnnonceInput() {
		return new QueueChannel();
	}

	//@Output(ADD_ANNONCE_OUTPUT)
	@Bean
	MessageChannel addAnnonceOutput() {
		return new QueueChannel();
	}
}
