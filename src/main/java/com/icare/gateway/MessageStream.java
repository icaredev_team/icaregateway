package com.icare.gateway;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;

import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created by ahadri on 1/22/17.
 */
public interface MessageStream{
	String ADD_ANNONCE_INPUT= "addAnnonceInput";
	String ADD_ANNONCE_OUTPUT = "addAnnonceOutput";

	@Input(ADD_ANNONCE_INPUT)
	SubscribableChannel addAnnonceInput();
	@Output(ADD_ANNONCE_OUTPUT)
	SubscribableChannel addAnnonceOutput();


	String ACCEPT_DEMANDE_OUTPUT = "acceptDemandeOutput";
	String ACCEPT_DEMANDE_INPUT = "acceptDemandeInput";
	@Output(ACCEPT_DEMANDE_OUTPUT)
	SubscribableChannel acceptDemandeOutput();
	@Input(ACCEPT_DEMANDE_INPUT)
	SubscribableChannel acceptDemandeInput();


	String UPDATE_CONDUCTEUR_OUTPUT = "updateConducteurOutput";
	@Output(UPDATE_CONDUCTEUR_OUTPUT)
	SubscribableChannel updateConducteurOutput();

	String ADD_CONDUCTEUR_OUTPUT= "addConducteurOutput";
	@Output(ADD_CONDUCTEUR_OUTPUT)
	SubscribableChannel addConducteurOutput();
}
