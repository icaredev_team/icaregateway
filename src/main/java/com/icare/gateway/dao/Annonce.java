package com.icare.gateway.dao;


/**
 * Created by ahadri on 1/20/17.
 */
public class Annonce {

	private int id;
	private int prix;
	private int nbrePlaces;
	private String date;

	@Override
	public String toString() {
		return "Annonce{" +
					   "id=" + id +
					   ", prix=" + prix +
					   ", nbrePlaces=" + nbrePlaces +
					   ", date='" + date + '\'' +
					   '}';
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public int getNbrePlaces() {
		return nbrePlaces;
	}

	public void setNbrePlaces(int nbrePlaces) {
		this.nbrePlaces = nbrePlaces;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
