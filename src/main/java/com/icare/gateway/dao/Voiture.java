package com.icare.gateway.dao;


/**
 * Created by ahadri on 1/20/17.
 */
public class Voiture {

	private String matricule;
	private String couleur;
	private String modele;

	@Override
	public String toString() {
		return "Voiture{" +
					   " matricule='" + matricule + '\'' +
					   ", couleur='" + couleur + '\'' +
					   ", modele='" + modele + '\'' +
					   '}';
	}

	public Voiture() {
	}

	public Voiture(String matricule, String couleur, String modele) {
		this.matricule = matricule;
		this.couleur = couleur;
		this.modele = modele;
	}
	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}
}
