package com.icare.gateway.dao;

/**
 * Created by ahadri on 1/20/17.
 */
public class Conducteur {

	private Long id;
	private Voiture voiture;
	private String nom;
	private String prenom;
	private String cin;
	private String telephone;
	private String email;
	private String age;

	@Override
	public String toString() {
		return "Conducteur{" +
					   "id=" + id +
					   ", voiture=" + voiture +
					   ", nom='" + nom + '\'' +
					   ", prenom='" + prenom + '\'' +
					   ", cin='" + cin + '\'' +
					   ", telephone='" + telephone + '\'' +
					   ", email='" + email + '\'' +
					   ", age='" + age + '\'' +
					   '}';
	}

	public Conducteur() {
	}


	public Conducteur(String nom, String prenom, String cin, String telephone, String email, String age, Voiture voiture) {
		this.nom = nom;
		this.prenom = prenom;
		this.cin = cin;
		this.telephone = telephone;
		this.email = email;
		this.age = age;
		this.voiture = voiture;
	}

	public Voiture getVoiture() {
		return voiture;
	}

	public void setVoiture(Voiture voiture) {
		this.voiture = voiture;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

}
