package com.icare.gateway.dao;


/**
 * Created by ahadri on 1/20/17.
 */
public class Trajet {

	private String depart;
	private String destination;


	public Trajet() {
	}

	public Trajet(String depart, String destination) {
		this.depart = depart;
		this.destination = destination;
	}

	@Override
	public String toString() {
		return "Trajet{" +
					   " depart='" + depart + '\'' +
					   ", destination='" + destination + '\'' +
					   '}';
	}

	public String getDepart() {
		return depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
}



